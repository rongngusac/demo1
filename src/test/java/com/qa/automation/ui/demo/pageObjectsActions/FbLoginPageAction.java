package com.qa.automation.ui.demo.pageObjectsActions;

import com.qa.automation.ui.demo.pageObjects.FbLoginPage;
import org.openqa.selenium.WebDriver;

public class FbLoginPageAction extends FbLoginPage {
    public FbLoginPageAction(WebDriver webDriver) {
        super(webDriver);
    }

    public void fillEmailFields(String email) {
        this.getEmailField().sendKeys(email);
    }

    public void fillPasswordField(String password) {
        this.getPassField().sendKeys(password);
    }

    public void clickLoginButton() {
        this.getLoginButton().click();
    }
}
