package com.qa.automation.ui.demo.testcases;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC01_LoginFbAccount extends UiTestCaseBase {

    @BeforeClass
    public static void init() {
        initialize();
    }

    @Test
    public void goToFbLoginPage() {
        browseURL("https://www.facebook.com/");
    }

    @Test(dependsOnMethods={"goToFbLoginPage"})
    public void fillEmailInfo() {
        fbLoginPageAction.fillEmailFields("rongngusac2008@gmail.com");
    }

    @Test(dependsOnMethods = {"fillEmailInfo"})
    public void fillPasswordInfo() {
        fbLoginPageAction.fillPasswordField("kimdong2402243");
    }

    @Test(dependsOnMethods = {"fillPasswordInfo"})
    public void clickOnLoginButton() {
        fbLoginPageAction.clickLoginButton();
    }
}
