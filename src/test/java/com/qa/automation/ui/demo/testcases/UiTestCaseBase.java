package com.qa.automation.ui.demo.testcases;

import com.qa.automation.ui.demo.pageObjectsActions.FbLoginPageAction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UiTestCaseBase {
    public WebDriver driver;
    public String url = "https://www.gmail.com/";
    public FbLoginPageAction fbLoginPageAction;

    public static void initialize() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nbl24\\.m2\\repository\\webdriver\\chromedriver\\win32\\86.0.4240.75\\chromedriver.exe");
    }

    public void browseURL(String url) {
        driver =  new ChromeDriver();
        fbLoginPageAction = new FbLoginPageAction(driver);
        //browseURL(url);

        driver.get(url);
        driver.manage().window().maximize();
    }
}
