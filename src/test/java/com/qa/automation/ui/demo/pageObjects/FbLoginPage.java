package com.qa.automation.ui.demo.pageObjects;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
public class FbLoginPage {
    public WebDriver driver;

    @FindBy(xpath = "//form[contains(@class, 'formContainer')]//input[@id='email']")
    WebElement emailField;

    @FindBy(xpath = "//form[contains(@class, 'formContainer')]//input[@id='pass']")
    WebElement passField;

    @FindBy(xpath = "//form[contains(@class, 'formContainer')]//button[@name='login']")
    WebElement loginButton;


    public FbLoginPage(WebDriver webDriver) {
        this.driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
}
